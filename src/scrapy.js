import { URL } from 'url';
import { Log } from './services/log-service';
import { HttpService as Http } from './services/http-service';

export class Scrapy {

  /**
   * Master class which crawls the urls
   * 
   * Class members:
   *
   * callback: callback which will be called for each URL crawled.
   *
   * done: Boolean for aborting the crawler, if true then crawler will not continue crawling.
   *
   * http: Instance of class HttpService.
   *
   * maxRequests: Maximum number of requests which can be alive within this crawler.
   * This will be incremented if a request is initated and decremented if request
   * Completes.
   * 
   * numberOfRequests: Keep track for number of requests inititated.
   *
   * traversed: Keep track of traversed URLs by this crawler to prevent crawling them again.
   */

  constructor(callback, maxRequests) {
    //Initializing class members.

    this.callback = callback;
    
    this.done = false;
    
    this.http = new Http;
    
    this.maxRequests = maxRequests;
    
    this.numberOfRequests = 0;

    this.traversed = [];
  }
  
  crawl(url) {
    this.numberOfRequests++;

    let urlObject;

    try {
      // URL class raises error if url is Invalid.
      urlObject = new URL(url);
    } catch(e) {
      Log.error('Cannot crawl this URL:' + url + ', Invalid URL');
    }

    Log.info('Crawling this URL:' + url);

    //Initiating the request.
    this.http.get(url)
    .then(($) => {
      this.numberOfRequests--;

      /**
       * Time to call this URL.
       * Since request is completed now.
       */

      this.callback(null, $, () => { this.done = true });

      //Checking if quit() is called 
      if(!this.done) {
        //Looping through all the anchor tags on the page.
        let urls = $('a').toArray().map((tag) => {
          
          let url = $(tag).attr('href');

          if(!url) {
            return;
          }

          //checking if already traversed.
          if(this.traversed.indexOf(url) >= 0) {
            return;
          } else {
            this.traversed.push(url);
          }

          /**
           * Handling various scenarios 
           * where URL starts with '//' or '/'
           * Ignoring those URLs targetting '#' or 'javascript:'
           */

          if(url.startsWith('//')) {
            url = urlObject.protocol + url;
          } 
          else if(url.startsWith('/')) {
            url = urlObject.href + url;
          } 
          else if(url.startsWith('#') || url.startsWith('javascript:')) {
            return;
          }
          
          /**
           * Since if the current url is https://python.org/xyz/abc then
           * <a href="profile" ...> 
           * will target https://python.org/xyz/profile 
           */

          else if(!url.startsWith('http://') && !url.startsWith('https://')){
            let pieces = urlObject.href.split('/');

            pieces[pieces.length - 1] = url;

            url = pieces.join('/');
          }

          //removing '#' part in last.
          url = url.split('#')[0];

          urlObject = new URL(url);
          urlObject.pathname = urlObject.pathname.replace(/\/\/+/i, '/');

          url = urlObject.href;

          /**
           * Checking if number of requests is greater than maximum limit of live requests.
           * checking on a very short interval every time and clearing if condition meets.
           */

          let eventLoop = setInterval(() => {
            if(this.numberOfRequests < this.maxRequests) {
              clearInterval(eventLoop);
              
              this.crawl(url);
            }
          }, 100);
        });
      }
    })
    .catch(error => {
      this.numberOfRequests--;

      //calling the callback with error parameter.
      this.callback(error);

      Log.error('Error crawling "' + url + '" URL' + error.toString());
    });
  }
}