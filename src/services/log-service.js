import fs from 'fs';

/**
 * A simple Logger class containing 
 * functions to appends log into a file
 * with Log severity as 'info' or 'error'
 */

export class Log {
  /**
   * Appends a line in the log file.
   * 
   * @param String: string to append in file.
   */

  static write(string) {
    let LOG_FILE_PATH = __dirname + '/logs';

    if(!fs.existsSync(LOG_FILE_PATH)) {
      fs.mkdirSync(LOG_FILE_PATH);
    }

    LOG_FILE_PATH += '/scrapy.log';

    let log_file = fs.createWriteStream(LOG_FILE_PATH, {flags : 'a'});

    log_file.write(string + '\n');
  }

  /**
   * Appends a line in the log file with severity as 'info'.
   * 
   * @param String: string to append in file.
   */

  static info(data) {
    Log.write('[' + (new Date).toISOString() + ']' + '[INFO]' + data);
  }

  /**
   * Appends a line in the log file with severity as 'error'.
   * 
   * @param String: string to append in file.
   */

  static error(data) {
    Log.write('[' + (new Date).toISOString() + ']' + '[ERROR]' + data);
  }
}