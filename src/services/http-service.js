import request from 'request';
import cheerio from 'cheerio';

/**
 * This class simply initiates a HTTP request 
 * and returns a promise if resolves returns a cheerio object.
 * and error if rejected.
 */

export class HttpService {

  /**
   * initiates a 'GET' request to server
   *
   * @param String url
   * @return Promise
   */
   
  get(url) {
    return new Promise((resolve, reject) => {

      request(url, (error, response, html) => {
        if(!error) {    
          let $ = cheerio.load(html);

          resolve($);
        
        } else {
          // Rejecting if Error occurs
          reject(error);
        }

      });
    });
  }
}