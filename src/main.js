import { Scrapy } from './scrapy';

/**
 * Example use of Scrapy class
 * 
 * Defined a callback required for every URL and maximum number of requests as 10
 * err will be null if request succeeds, $ is the cheerio object for the response
 * HTML, quit can be called to abort all other requests.
 *
 * This example callback simply parses the title from the HTML response and quits after 10 requests.
 */

var requestsCount = 0;
function scrape(err, $, quit) {

  if(!err) {
    console.log('Scraped Title:', $('title').text());

      requestsCount++;

      if(requestsCount >= 10) {
        quit();
      }
  } else {
    console.log(err);
  }
}

let scrapy = new Scrapy(scrape, 10);

//crawling python.org
scrapy.crawl('http://python.org');