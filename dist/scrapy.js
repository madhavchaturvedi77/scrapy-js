'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Scrapy = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _url = require('url');

var _logService = require('./services/log-service');

var _httpService = require('./services/http-service');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Scrapy = exports.Scrapy = function () {

  /**
   * Master class which crawls the urls
   * 
   * Class members:
   *
   * callback: callback which will be called for each URL crawled.
   *
   * done: Boolean for aborting the crawler, if true then crawler will not continue crawling.
   *
   * http: Instance of class HttpService.
   *
   * maxRequests: Maximum number of requests which can be alive within this crawler.
   * This will be incremented if a request is initated and decremented if request
   * Completes.
   * 
   * numberOfRequests: Keep track for number of requests inititated.
   *
   * traversed: Keep track of traversed URLs by this crawler to prevent crawling them again.
   */

  function Scrapy(callback, maxRequests) {
    _classCallCheck(this, Scrapy);

    //Initializing class members.

    this.callback = callback;

    this.done = false;

    this.http = new _httpService.HttpService();

    this.maxRequests = maxRequests;

    this.numberOfRequests = 0;

    this.traversed = [];
  }

  _createClass(Scrapy, [{
    key: 'crawl',
    value: function crawl(url) {
      var _this = this;

      this.numberOfRequests++;

      var urlObject = void 0;

      try {
        // URL class raises error if url is Invalid.
        urlObject = new _url.URL(url);
      } catch (e) {
        _logService.Log.error('Cannot crawl this URL:' + url + ', Invalid URL');
      }

      _logService.Log.info('Crawling this URL:' + url);

      //Initiating the request.
      this.http.get(url).then(function ($) {
        _this.numberOfRequests--;

        /**
         * Time to call this URL.
         * Since request is completed now.
         */

        _this.callback(null, $, function () {
          _this.done = true;
        });

        //Checking if quit() is called 
        if (!_this.done) {
          //Looping through all the anchor tags on the page.
          var urls = $('a').toArray().map(function (tag) {

            var url = $(tag).attr('href');

            if (!url) {
              return;
            }

            //checking if already traversed.
            if (_this.traversed.indexOf(url) >= 0) {
              return;
            } else {
              _this.traversed.push(url);
            }

            /**
             * Handling various scenarios 
             * where URL starts with '//' or '/'
             * Ignoring those URLs targetting '#' or 'javascript:'
             */

            if (url.startsWith('//')) {
              url = urlObject.protocol + url;
            } else if (url.startsWith('/')) {
              url = urlObject.href + url;
            } else if (url.startsWith('#') || url.startsWith('javascript:')) {
              return;
            }

            /**
             * Since if the current url is https://python.org/xyz/abc then
             * <a href="profile" ...> 
             * will target https://python.org/xyz/profile 
             */

            else if (!url.startsWith('http://') && !url.startsWith('https://')) {
                var pieces = urlObject.href.split('/');

                pieces[pieces.length - 1] = url;

                url = pieces.join('/');
              }

            //removing '#' part in last.
            url = url.split('#')[0];

            urlObject = new _url.URL(url);
            urlObject.pathname = urlObject.pathname.replace(/\/\/+/i, '/');

            url = urlObject.href;

            /**
             * Checking if number of requests is greater than maximum limit of live requests.
             * checking on a very short interval every time and clearing if condition meets.
             */

            var eventLoop = setInterval(function () {
              if (_this.numberOfRequests < _this.maxRequests) {
                clearInterval(eventLoop);

                _this.crawl(url);
              }
            }, 100);
          });
        }
      }).catch(function (error) {
        _this.numberOfRequests--;

        //calling the callback with error parameter.
        _this.callback(error);

        _logService.Log.error('Error crawling "' + url + '" URL' + error.toString());
      });
    }
  }]);

  return Scrapy;
}();