'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HttpService = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * This class simply initiates a HTTP request 
 * and returns a promise if resolves returns a cheerio object.
 * and error if rejected.
 */

var HttpService = exports.HttpService = function () {
  function HttpService() {
    _classCallCheck(this, HttpService);
  }

  _createClass(HttpService, [{
    key: 'get',


    /**
     * initiates a 'GET' request to server
     *
     * @param String url
     * @return Promise
     */

    value: function get(url) {
      return new Promise(function (resolve, reject) {

        (0, _request2.default)(url, function (error, response, html) {
          if (!error) {
            var $ = _cheerio2.default.load(html);

            resolve($);
          } else {
            // Rejecting if Error occurs
            reject(error);
          }
        });
      });
    }
  }]);

  return HttpService;
}();