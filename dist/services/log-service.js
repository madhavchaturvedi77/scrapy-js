'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Log = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * A simple Logger class containing 
 * functions to appends log into a file
 * with Log severity as 'info' or 'error'
 */

var Log = exports.Log = function () {
  function Log() {
    _classCallCheck(this, Log);
  }

  _createClass(Log, null, [{
    key: 'write',

    /**
     * Appends a line in the log file.
     * 
     * @param String: string to append in file.
     */

    value: function write(string) {
      var LOG_FILE_PATH = __dirname + '/logs';

      if (!_fs2.default.existsSync(LOG_FILE_PATH)) {
        _fs2.default.mkdirSync(LOG_FILE_PATH);
      }

      LOG_FILE_PATH += '/scrapy.log';

      var log_file = _fs2.default.createWriteStream(LOG_FILE_PATH, { flags: 'a' });

      log_file.write(string + '\n');
    }

    /**
     * Appends a line in the log file with severity as 'info'.
     * 
     * @param String: string to append in file.
     */

  }, {
    key: 'info',
    value: function info(data) {
      Log.write('[' + new Date().toISOString() + ']' + '[INFO]' + data);
    }

    /**
     * Appends a line in the log file with severity as 'error'.
     * 
     * @param String: string to append in file.
     */

  }, {
    key: 'error',
    value: function error(data) {
      Log.write('[' + new Date().toISOString() + ']' + '[ERROR]' + data);
    }
  }]);

  return Log;
}();